gitのセットアップ
===

以降はすべてUbuntu内で行う。

## gitのバージョンアップ
Ubuntuにはデフォルトでgitが入っているがちょっと古いので最新版をインストールする

以下コマンド実行
```sh
# 現在のバージョンを確認
git --version  # => git version 2.17.1

# git update
sudo add-apt-repository ppa:git-core/ppa
sudo apt update
sudo apt install git -y

# 再度確認
git --version # => git version 2.27.0
```

## git configの設定
gitではcommitするためにユーザ名とメールアドレスを設定しないといけない。

以下コマンドを実行
```sh
# nameの設定
git config --global user.name "jagio" # 名前は日本語を使わないように
# emailの設定
git config --global user.email "jagio@example.com"
```

## ssh-keyの作成
Gitlabにアップロードされているリポジトリとやり取りするためにsshキーが必要となる

1. `ssh-keygen`
  - 色々聞かれるので全部Enter押す
2. ~/.ssh/にid_rsa.pubファイルができているので、中身をコピーして自分のGialabアカウントに凍露する。
  - [ここらへん](https://qiita.com/CUTBOSS/items/462a2ed28d264aeff7d5#ssh%E8%AA%8D%E8%A8%BC%E3%82%AD%E3%83%BC%E5%85%AC%E9%96%8B%E9%8D%B5%E3%82%92%E7%99%BB%E9%8C%B2%E3%81%99%E3%82%8B)を参考にするとよい
3. boatプロジェクトの適当なリポジトリ(build-envとかgit-practice)をcloneする
  - https://gitlab.com/boat1
  - 画面右上の青いcloneボタンをクリックしてURLをコピー
  - `git clone <URL>`でcloneできる
    - うまく行っていればリポジトリの中身がダウンロードできているはず
