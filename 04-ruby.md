Rubyの導入
===

```sh
# 前準備
apt-get install autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm5 libgdbm-dev

# rbenvインストール
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bash_profile
~/.rbenv/bin/rbenv init
source ~/.bash_profile

# rbenvを使ってruby 2.7.0をインストールする
rbenv install 2.7.0
rbenv global 2.7.0

# 確認
ruby -v
# => ruby 2.7.0p0 (2019-12-25 revision 647ee6f091) [x86_64-darwin19]
```
